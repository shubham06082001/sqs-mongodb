const AWS = require("aws-sdk");
const mongoose = require("mongoose");
const express = require("express");
const serverless = require("serverless-http")

AWS.config.update({
  accessKeyId: "AKIA2KOPSPMVZOJH5MUU",
  secretAccessKey: "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",
  region: "us-east-1",
})

const SQS_QUEUE_URL = "https://sqs.us-east-1.amazonaws.com/709641796395/legacy-queue-2309"
const MONGODB_URI =
  "mongodb+srv://shubhamkmr06082001:jl6RXqidtv3Eg1ET@database.mk1kzpb.mongodb.net/cache-legacy"

const sqs = new AWS.SQS({ region: "us-east-1" });

mongoose.connect(MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error:"));

db.once("open", async () => {
  console.log("Connected to MongoDB");

  const receiveParams = {
    QueueUrl: SQS_QUEUE_URL,
    MaxNumberOfMessages: 10,
    VisibilityTimeout: 30,
    WaitTimeSeconds: 0,
  };

  try {
    const sqsData = await sqs.receiveMessage(receiveParams).promise();

    if (sqsData.Messages) {
      for (const message of sqsData.Messages) {
        const body = JSON.parse(message.Body);
        const Message = mongoose.model("Message", new mongoose.Schema({
          messageId: String,
          messageBody: String,
        }));

        const newMessage = new Message({
          messageId: message.MessageId,
          messageBody: JSON.stringify(body),
        });

        try {
          await newMessage.save();
          console.log("Saved message to MongoDB:", newMessage);
        } catch (saveErr) {
          console.error("Error saving message to MongoDB:", saveErr);
        }

        await sqs.deleteMessage({
          QueueUrl: SQS_QUEUE_URL,
          ReceiptHandle: message.ReceiptHandle,
        }).promise();

        console.log("Deleted message from SQS:", message.MessageId);
      }
    }
  } catch (err) {
    console.error("Error receiving messages from SQS:", err);
  }
});

const app = express();
const port = 3000;

// app.listen(port, () => {
//   console.log(`Server is running on http://localhost:${port}`);
// });

module.exports.handler = serverless(app)
